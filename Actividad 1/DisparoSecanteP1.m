function [nodos, solaprox, t, iter, incre] = DisparoSecanteP1(funcion, a, b, alfa, beta, n, tol, maxiter)
    h = (b - a) / n;
    x = a:h:b;
    
    t0 = 0; % Inicialización de t0
    [x, Y] = ode45(funcion, x, [alfa, t0]');
    yb0 = Y(end, 1);
    ypb0 = Y(end, 2);
    
    t1 = (beta - alfa) / (b - a); % Inicialización de t1
    [x, Y] = ode45(funcion, x, [alfa, t1]');
    yb1 = Y(end, 1);
    ypb1 = Y(end, 2);
    
    iter = 0;
    incre = abs(yb1 + ypb1 - beta); % Criterio de convergencia
    
    while incre > tol && iter < maxiter
        t = t1 - ((t1 - t0) * (yb1 + ypb1 - beta)) / (yb1 + ypb1 - yb0 - ypb0); % Método de la secante
        % t=t1 -((t1 -t0)*(yb1-ypb1-beta))./(yb1-ypb1-yb0+ypb0);
        [x, Y] = ode45(funcion, x, [alfa, t]');
        yb0 = yb1;
        ypb0 = ypb1;
        
        yb1 = Y(end, 1);
        ypb1 = Y(end, 2);
        
        incre = abs(yb1 + ypb1 - beta);
        % incre =abs(yb1-ypb1-beta);
        iter = iter + 1;
        
        t0 = t1;
        t1 = t;
    end
    
    if incre <= tol
        nodos = x;
        solaprox = Y;
    else
        disp('Se necesitan más iteraciones.')
    end
end