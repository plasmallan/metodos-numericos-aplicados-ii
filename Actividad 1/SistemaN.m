function F = SistemaN(x, y)
    F = zeros(4, 1);
    F(1) = y(2);
    F(2) = 2*x*y(1) + 2 - y(1)*y(2);
    F(3) = y(4);
    F(4) = -y(1).*y(4)+2.*x.*y(3)-y(2).*y(3);
end