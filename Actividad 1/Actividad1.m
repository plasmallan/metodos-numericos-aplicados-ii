[xS ,yS,tS,iterS,increS] = DisparoSecanteP1(@SistemaS,1,2,1/2,17/2,10,1e-8,100);
[xN,yN,tN,iterN,increN] = DisparoNewtonP1(@SistemaN,1,2,1/2,17/2,9,1e-8,100);
figure;
plot(xS,yS(:,1));
xlabel("x")
ylabel("y")
figure;
plot(xN,yN(:,1));
xlabel("x")
ylabel("y")
figure;
hold on;
plot(xS,yS(:,1));
plot(xN,yN(:,1));
xlabel("x")
ylabel("y")
legend("Secante","Newton")
hold off;