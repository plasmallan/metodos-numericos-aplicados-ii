f=@(x,y,z)2 - z.^2 + 4*y;
fy=@(x,y,z)4;
fz=@(x,y,z)-2*z;
a=1;
b=2;
alfa=0;
beta=-3;
N=99;
maxiter=50;
tol=1e-5;
format long
[X,Y,iter ,incr, t_crout ]= mn2_2aDifnolin3 (f,fy ,fz ,a,b,alfa , beta ,N,maxiter ,tol);
%% Crout
disp('Últimas 10 aproximaciones con Crout:')
disp(Y(101-9:101))
hold on
plot(X,Y,'*-r')
fig = gcf;
saveas(fig, 'mn2_2aCrout.png')
%% Gauss
[X,Y_gauss,iter ,incr, t_gauss ]= mn2_2aGauss (f,fy ,fz ,a,b,alfa , beta ,N,maxiter ,tol);
disp('Últimas 10 aproximaciones con Gauss:')
disp(Y_gauss(end-9:end))
hold on
plot(X,Y_gauss,'*-b')

fig = gcf;
saveas(fig, 'mn2_2aGauss.png')
%% Tablas para las últimas 10 aproximaciones
last10_gauss = Y_gauss(end-9:end);
last10_crout = Y(end-9:end);

% Creamos una tabla con las últimas 10 aproximaciones
T = table(last10_gauss, last10_crout, 'VariableNames', {'Gauss', 'Crout'});
disp(T)

%% Comaparación de medias de tiempos

% Cálculo de los tiempos promedio de ejecución
promedio_gauss = mean(t_gauss);
promedio_crout = mean(t_crout);

% Mostrar los vectores de tiempos de ejecución
disp('Tiempos de ejecución con Gauss:')
disp(t_gauss)
disp('Tiempo promedio de ejecución con Gauss:')
disp(promedio_gauss)

disp('Tiempos de ejecución con Crout:')
disp(t_crout)
disp('Tiempo promedio de ejecución con Crout:')
disp(promedio_crout)

