function [X,Y,iter ,incr, t_gauss ]= mn2_2aGauss (f,fy ,fz ,a,b,alfa , beta ,N,maxiter ,tol)
% f = f(x,y,z) es la ecuacion diferencial
% fy = fy(x,y,z) es la parcial de f respecto a y
% fz = fz(x,y,z) es la parcial de f respecto a ... y'= z;
h=(b-a)/(N+1); 
k=(beta-alfa)/(N+1); %<- PROBLEMA
X=a:h:b; 
Y=alfa:k:beta; %<- PROBLEMA 
x=X(1:N+2); 
y=Y(1:N+2);

incr=tol +1; % Inicializar parametros
iter =0;
t_gauss = [];
while incr >tol && iter < maxiter
z=(Y(3:N+2)-Y(1:N))/(2* h); %Estimacion de la
%derivada por diferencias centrales ...
z=[-Y(1)/2 z (Y(end)-beta)/2]; %Cambio 
%evaluaciones
fe= feval (f,x,y,z);
fye= repmat(feval (fy ,x,y,z), 1, 101);
fze= feval (fz ,x,y,z);
%diagonales
dp =2+h^2*fye;
dp(1)=2-h+h^2*fye(1)-(h^2/2)*fze(1);
dp(end)=(2-h)+h^2*fye(end)+(h^2/2)*fze(end);

ds = -1+(h/2)*fze(1 :end -1);
ds(1)=-2;

di=-1-(h/2)*fze(2:end);
di(end)=-2;
% vecotr derecho
d(2:N+1)=-(-diff(Y,2)+h^2*fe(2:N+1));
d(1)=-((2-h)*y(1)-2*y(2)+h^2*fe(1));

d(N+2)=-( (2-h)*y(N+2)  -2*y(N+1) +h^2*fe(N+2)+beta*h);

%% llamamos a Gauss de Matlab 
% Con dp, ds, di, los vectores de las diagonales principal, superior e inferior
n = length(dp); 
% Construimos la matriz A
A = diag(dp) + diag(ds(1:n-1),1) + diag(di(1:n-1),-1);
% Resolvemos el sistema utilizando el operador de barra invertida
tic
v = A \ d';
t_gauss = [t_gauss; toc];

y=y+v';
Y=y;
incr=max(abs(v));
iter= iter +1;
end
X=X(:);
Y=Y(:);
end