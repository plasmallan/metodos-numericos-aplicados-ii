% Parámetros
alpha = 1;  % Constante en la ecuación
L = 1;     % Longitud del dominio espacial
nx = 10;   % Número de divisiones espaciales
Tmax = 0.5; % Tiempo máximo
nt = Tmax / 0.0005; % Número de divisiones temporales

% Condición inicial
ci = @(x) 0;

% Esquema Explícito
[u_exp, x, t_exp] = CalorExplND(alpha, ci, L, nx, Tmax, nt);

% Esquema Implícito
[U_imp, x] = CalorImplND(alpha, ci, 0, L, nx, Tmax, nt);

% Esquema Crank-Nicholson
[x, U_cn] = CrankNicholsonND(ci, 0, L, nx, Tmax, nt, alpha);

% Mostrar resultados en t = 0.5
disp('Solución en t = 0.5:')

% Tabla de resultados
results = table(x', u_exp(:, end), U_imp(:, end), U_cn(:, end), ...
                'VariableNames', {'x', 'Explícito', 'Implícito', 'Crank-Nicholson'});

disp(results)

% Representación gráfica
figure;
plot(x, u_exp(:, end), '-o', x, U_imp(:, end), '-x', x, U_cn(:, end), '-s')
legend('Explícito', 'Implícito', 'Crank-Nicholson')
title('Solución en t = 0.5')
xlabel('x')
ylabel('u(x,0.5)')

% Resolver para T = 1
Tmax = 1; % Tiempo máximo
nt = Tmax / 0.0005; % Número de divisiones temporales

% Esquema Explícito
[u_exp, x, t_exp] = CalorExplND(alpha, ci, L, nx, Tmax, nt);

% Esquema Implícito
[U_imp, x] = CalorImplND(alpha, ci, 0, L, nx, Tmax, nt);

% Esquema Crank-Nicholson
[x, U_cn] = CrankNicholsonND(ci, 0, L, nx, Tmax, nt, alpha);

% Representación gráfica para T = 1
figure;
plot(x, u_exp(:, end), '-o', x, U_imp(:, end), '-x', x, U_cn(:, end), '-s')
legend('Explícito', 'Implícito', 'Crank-Nicholson')
title('Solución en t = 1')
xlabel('x')
ylabel('u(x,1)')

% Tabla de resultados
disp('Solución en t = 1:')
results = table(x', u_exp(:, end), U_imp(:, end), U_cn(:, end), ...
                'VariableNames', {'x', 'Explícito', 'Implícito', 'Crank-Nicholson'});

disp(results)