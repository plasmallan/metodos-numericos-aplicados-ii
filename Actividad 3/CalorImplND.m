function [U, x] = CalorImplND(alpha, ci, a, b, nx, Tmax, nt)
    h = (b - a) / nx;
    x = a:h:b;
    k = Tmax / nt;
    t = 0:k:Tmax;

    U = zeros(nx+1, nt+1);
    cix = feval(ci, x);
    U(:, 1) = cix';

    lambda = alpha^2 * k / h^2;

    dp = (1 + 2 * lambda) * ones(nx+1, 1);
    ds = -lambda * ones(nx, 1);
    di = ds;

    dp(end) = (1 + 2 * lambda + 2 * lambda * h);
    ds(1) = -2 * lambda;
    di(end) = -2 * lambda;

    for j = 2:nt+1
        d = U(:, j-1) + k * t(j)^2 * U(:, j-1) + k * x' .* cos(x' * t(j));
        z = Crout(dp, ds, di, d);
        U(:, j) = z;
    end
end