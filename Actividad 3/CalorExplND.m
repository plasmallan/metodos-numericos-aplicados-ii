function [u, x, t] = CalorExplND(alpha, ci, L, nx, Tmax, nt)
    h = L / nx;
    x = 0:h:L;
    k = Tmax / nt;
    t = 0:k:Tmax;

    cix = feval(ci, x);
    u = zeros(nx+1, nt+1);
    u(:, 1) = cix';

    lambda = k * alpha^2 / h^2;
    if lambda > 1/2
        disp('No se cumple el criterio de convergencia')
    else
        disp('Sin problema')
    end

    for j = 1:nt
        u(1, j+1) = 2*lambda*u(2, j) + (1 - 2*lambda)*u(1, j) + k * t(j)^2 * u(1, j) + k * x(1) * cos(x(1) * t(j));
        for i = 2:nx
            u(i, j+1) = lambda * (u(i-1, j) + u(i+1, j)) + (1 - 2*lambda) * u(i, j) + k * t(j)^2 * u(i, j) + k * x(i) * cos(x(i) * t(j));
        end
        u(nx+1, j+1) = 2*lambda * u(nx, j) + (1 - 2*lambda * (1 + h)) * u(nx+1, j) + k * sin(t(j+1));
    end
end
