# Métodos Numéricos Aplicados II - PER 8406 Octubre 2023

Repositorio en que se almacenan los códigos de Matlab recibidos y realizados en el curso de Métodos Numéricos Aplicados II de la Maestría en Ingeniería Matemática de la Universidad Internacional de la Rioja.